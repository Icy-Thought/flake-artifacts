{
  options,
  config,
  inputs,
  lib,
  pkgs,
  ...
}:

let
  diskoConf = "${config.snowflake.hostDir}/partition.nix";
in
with lib;
{
  imports = [
    inputs.disko.nixosModules.default
  ] ++ optional (builtins.pathExists diskoConf) (import diskoConf { });
}

# add to flake inputs:
# disko = {
#   url = "github:nix-community/disko";
#   inputs.nixpkgs.follows = "nixpkgs";
# }
