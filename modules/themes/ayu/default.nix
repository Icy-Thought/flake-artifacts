{
  options,
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.modules.themes;
in
with lib;
{
  config = mkIf (cfg.active == "ayu") (mkMerge [
    {
      modules.themes = {
        wallpaper = mkDefault ./wallpaper.jpg; # FIXME

        gtk = {
          name = "Orchis-Dark-Compact";
          package = pkgs.orchis-theme;
        };

        iconTheme = {
          name = "WhiteSur-dark";
          package = pkgs.whitesur-icon-theme;
        };

        pointer = {
          name = "Bibata-Modern-Classic";
          package = pkgs.bibata-cursors;
        };

        font = {
          packages = with pkgs; [
            libre-baskerville
            maple-mono-SC-NF
            noto-fonts-emoji
          ];
          mono.family = "Maple Mono NF CN";
          sans.family = "Maple Mono NF CN";
          emoji.family = "Noto Color Emoji";
        };

        colors = {
          main = {
            normal = {
              black = "#1d242c";
              red = "#ff7733";
              green = "#b8cc52";
              yellow = "#ffb454";
              blue = "#36a3d9";
              magenta = "#ca30c7";
              cyan = "#95e6cb";
              white = "#c7c7c7";
            };
            bright = {
              black = "#686868";
              red = "#f07178";
              green = "#cbe645";
              yellow = "#ffee99";
              blue = "#6871ff";
              magenta = "#ff77ff";
              cyan = "#a6fde1";
              white = "#ffffff";
            };
            types = {
              fg = "#e6e1cf";
              bg = "#0f1419";
              panelbg = "#171b24";
              border = "#f29718";
              highlight = "#e6e1cf";
            };
          };

          rofi = with cfg.colors.main; {
            bg = {
              main = types.bg;
              alt = "#0D1012";
              bar = "#1A1D23";
            };
            fg = types.fg;
            ribbon = {
              outer = "#2A3A4A";
              inner = "#1C8DB4";
            };
            selected = "#1C5A8C";
            urgent = "#FF8000";
          };
        };

        editor = {
          helix = {
            dark = "ayu_dark";
            light = "ayu_light";
          };
          neovim = {
            dark = "ayu-dark";
            light = "ayu-light";
          };
        };
      };
    }

    # (mkIf config.modules.desktop.browsers.firefox.enable {
    #   firefox.userChrome =
    #     concatMapStringsSep "\n" readFile
    #     ["${configDir}" /firefox/userChrome.css];
    # })

    (mkIf config.services.xserver.enable {
      hm.programs.sioyek.config = with cfg.font.mono; {
        "custom_background_color " = "";
        "custom_text_color " = "";

        "text_highlight_color" = "";
        "visual_mark_color" = "";
        "search_highlight_color" = "";
        "link_highlight_color" = "";
        "synctex_highlight_color" = "";

        "page_separator_width" = "2";
        "page_separator_color" = "";
        "status_bar_color" = "";

        "font_size" = "${builtins.toString size}";
        "ui_font" = "${family} ${weight}";
      };
    })
  ]);
}
